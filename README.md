CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Notes
* Issues

INTRODUCTION
------------

Adds a field for storing Burgerservicenummers.

RECOMMENDED MODULES
-------------------

* Webform encrypt (https://www.drupal.org/project/webform_encrypt):
  Provides the ability to encrypt webform data in the database.

NOTES
-----

This module **only**, provides a field with validation and webform integration.
The data itself is stored plaintext by default. When using this module it is
the responsibility of the site builder to ensure the stored information is stored
securely, for example by using the webform_encrypt module.

ISSUES
------

Please report issues at https://www.drupal.org/project/issues/bsn_field.
