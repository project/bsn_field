<?php

namespace Drupal\bsn_field\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;

/**
 * Provides a form element for the input of BSN numbers.
 *
 * @FormElement("bsn")
 */
class BSNElement extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = get_class($this);
    $info['#maxlength'] = 9;
    $info['#pattern'] = '[0-9]{8,9}';
    $info['#element_validate'] = [
      [$class, 'validateBsn'],
    ];
    return $info;
  }

  /**
   * Validate the provided BSN.
   */
  public static function validateBsn(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#value']) && !_bsn_elfproef($element['#value'])) {
      $form_state->setError($element, t('Provide a valid BSN number.'));
    }
  }

}
