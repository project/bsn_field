<?php

namespace Drupal\bsn_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'bsn_default' widget.
 *
 * @FieldWidget(
 *   id = "bsn_default",
 *   label = @Translation("BSN number"),
 *   field_types = {
 *     "bsn"
 *   }
 * )
 */
class BSNDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'bsn',
      '#default_value' => $items[$delta]->value ?? '',
    ];

    return $element;
  }

}
