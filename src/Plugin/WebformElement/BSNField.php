<?php

namespace Drupal\bsn_field\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\TextBase;

/**
 * Provides a 'bsn' element.
 *
 * @WebformElement(
 *   id = "bsn",
 *   label = @Translation("BSN field"),
 *   description = @Translation("Provides a form element for input of a BSN number."),
 *   category = @Translation("Basic elements"),
 * )
 */
class BSNField extends TextBase {

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'minlength' => '8',
      'maxlength' => '9',
      'autocomplete' => 'on',
      'pattern' => '[0-9]{8,9}',
    ] + parent::defineDefaultProperties() + $this->defineDefaultMultipleProperties();
  }

}
