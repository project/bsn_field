<?php

namespace Drupal\Tests\bsn_field\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the `bsn_field` module.
 *
 * @group bsn_field
 */
class BSNTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    require_once __DIR__ . '/../../../bsn_field.module';
  }

  /**
   * Test the result of the bsn elfproef.
   *
   * @dataProvider elfProefData
   */
  public function testElfProef(string $bsn, bool $expected) {
    $this->assertEquals($expected, _bsn_elfproef($bsn));
  }

  /**
   * Return a BSN testset.
   */
  public function elfProefData(): array {
    return [
      // Valid.
      // Starts with a `0`.
      ['069176978', TRUE],
      // 8 numbers.
      ['69176978', TRUE],
      // 9 numbers.
      ['157132973', TRUE],
      // Invalid.
      ['6917697', FALSE],
      ['691769782', FALSE],
      ['157032973', FALSE],
    ];
  }

}
